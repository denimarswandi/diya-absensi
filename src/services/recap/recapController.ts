import { Request, Response } from "express";
import { Ptk } from "../ptk/model";
import { RecapAbsensi } from "./recapModel";
import { labelRecap } from "./lsitMonth";

class RecapAbsensiController{
    public async getAll(req:Request, res:Response):Promise<Response>{
        const lembaga = req.params.lembaga
        const kodeRekap = req.params.kodeRekap
        const data = await Ptk.findAll({
            include:[
                {model:RecapAbsensi, attributes:['ket', 'tanggal'], where:{
                    kodeRekap:kodeRekap
                }}
            ],
            where:{unitKerja:lembaga},
            order:[[RecapAbsensi, 'tanggal', 'ASC']],
            attributes:['uuid', 'nupy', 'nama', 'unitKerja']
        })
       return res.json(data)
    }
    public async listMonth(req:Request, res:Response):Promise<Response>{
        return res.json(labelRecap)
    }
}

export default new RecapAbsensiController
import { adminAuth, checkSuperAdmin } from "../../middleware/admin";
import BaseRouter from "../../router/baseRouter";
import Card from "./cardController";


class CardRouter extends BaseRouter{
    routes(): void {
        this.router.post('/:uuid', adminAuth, checkSuperAdmin, Card.create)
    }
}

export default new CardRouter().router
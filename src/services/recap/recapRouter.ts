import { adminAuth } from "../../middleware/admin";
import BaseRouter from "../../router/baseRouter";
import Recap from './recapController'

class RecapRouter extends BaseRouter{
    routes(): void {
        this.router.get('/:lembaga/:kodeRekap', adminAuth,Recap.getAll)
        this.router.get('/list/month/get', Recap.listMonth)
    }
}

export default new RecapRouter().router
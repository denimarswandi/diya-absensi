import { DataTypes } from "sequelize";
import db from '../../config/database';
import { Absensi } from "../absensi/absensiModel";

export const Card = db.define('card', {
    uuid:{
        type:DataTypes.UUID,
        primaryKey:true,
        defaultValue:DataTypes.UUIDV4
    },
    tagCode:{
        type:DataTypes.STRING(255),
        allowNull:false
    }
})

Card.hasOne(Absensi, {foreignKey:'card_uuid'})
Absensi.belongsTo(Card, {foreignKey:'card_uuid'})
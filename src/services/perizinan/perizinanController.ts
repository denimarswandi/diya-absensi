import { Request, Response} from "express";
import {Perizinan} from "./perizinanModel";
import { Op } from "sequelize";

class PerizinanController{
    public async getAll(req:Request, res:Response):Promise<Response>{
        return res.json({msg:'hello'})
    }
    public async create(req:Request, res:Response):Promise<Response>{
        const {ket, dariTanggal, sampaiTanggal} = req.body;
        const uuid = req.params.uuid
        const izin:any = await Perizinan.findOne({
            where:{
                ptk_uuid:uuid,
                [Op.or]:[
                  {[Op.and]:[
                    {
                        dariTanggal:{[Op.lte]:dariTanggal},
                        sampaiTanggal:{[Op.gte]:dariTanggal}
                    }
                  ]},
                  {
                    [Op.and]:[
                        {
                            dariTanggal:{[Op.lte]:sampaiTanggal},
                            sampaiTanggal:{[Op.gte]:sampaiTanggal}
                        }
                    ]
                  }
                ],
                
                
            }
        })
        // console.log(izin)
        if(izin){
            return res.status(403).json({msg:'data exist'})
        }
        await Perizinan.create({
            ket, dariTanggal, sampaiTanggal, ptk_uuid:uuid
        })
        return res.json({msg:'created'})
    }
}

export default new PerizinanController;
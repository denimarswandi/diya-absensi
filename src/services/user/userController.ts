import { Request, Response } from "express";
import { User } from "./userModel";
import Auth from '../../util/auth'

interface UserInterface {
    nama:string;
    username:string;
    password:string;
    lembaga:string;
    admin:boolean;
}

class UserController{
    public async register(req:Request, res:Response):Promise<Response>{
        let user:UserInterface = req.body;
        if(await User.findOne({where:{
            username:user.username
        }})){
            return res.status(400).json({msg:'user exist'})
        }
        const passwordHash:string = await Auth.passwordHash(user.password)
        await User.create({
            nama:user.nama,
            username:user.username,
            password:passwordHash,
            lembaga:user.lembaga,
            admin:user.admin
        })
        return res.json({msg:'created'})

    }

    public async login(req:Request, res:Response):Promise<Response>{
        const {username, password} = req.body;
        const user:any = await User.findOne({where:{username}})
        if(user){
            let compare = await Auth.passwordCompare(password, user.password)
            if(compare){
                let token =  Auth.generateToken(user.uuid)
                return res.json({
                    token:token,
                    username:user.username,
                    nama:user.nama,
                    admin:user.admin,
                    lembaga:user.lembaga
                })
            }
            return res.status(403).json({msg:'password wrong'})
        }
        return res.status(404).json({msg:'user not found'})

    }
    public async get(req:Request, res:Response):Promise<Response>{
        const user:any = await User.findAll({
            attributes:['uuid', 'nama', 'username', 'lembaga', 'admin']
        });
        return res.json(user)
    }
}

export default new UserController

import { adminAuth, checkSuperAdmin } from "../../middleware/admin";
import BaseRouter from "../../router/baseRouter";
import Jadwal from './jadwalController'
import { jadwalCreateValidator } from "./jadwalValidator";
class JadwalRouter extends BaseRouter{
    routes(): void {
        this.router.get('/:uuid', adminAuth, checkSuperAdmin, Jadwal.getAll)
        this.router.post('/:uuid', adminAuth, checkSuperAdmin, jadwalCreateValidator,Jadwal.create)

    }
}

export default new JadwalRouter().router
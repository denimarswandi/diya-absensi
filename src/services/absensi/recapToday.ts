import { Ptk } from "../ptk/model";
import { Absensi } from "./absensiModel";
import { Op } from "sequelize";

export const recapToday = (start:any, end:any):Promise<any>=>{
    return new Promise(async(resolve, _)=>{
      const data:any = await Ptk.findAll({
        include:[
            {
                model:Absensi,
                where:{
                    createdAt:{[Op.between]: [start, end]},
                },
                attributes:['absenTime']
            }
        ],
        order:[[Absensi, 'absenTime', 'ASC']],
        attributes:['nupy','nama', 'unitKerja']
      })
      let recapToday:any = []
      data.map((d:any)=>{
        let datang = null
        let pulang = null
        if(d.absensis.length>1){
            datang = d.absensis[0].absenTime
            pulang = d.absensis[d.absensis.length-1].absenTime
        }else if (d.absensis.length===1){
            datang = d.absensis[0].absenTime
            pulang = '-'
        }
        recapToday.push({nupy:d.nupy, lembaga:d.unitKerja ,nama:d.nama, datang:datang, pulang:pulang})
      })
      resolve(recapToday)
    })
}
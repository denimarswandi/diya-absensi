import db from "./database";
import { Ptk } from "../services/ptk/model";
import { Absensi } from "../services/absensi/absensiModel";
import {noUrutPegawai} from '../services/nupy/noUrutModel'
import { Card } from "../services/card/cardModel";
import { Jadwal } from "../services/jadwal/jadwalModel";
import { ListJadwal } from "../services/lsitJadwal/listJadwalModel";
import { JadwalSementara } from "../services/jadwalSementara/jadwalSementaraModel";
import { User } from "../services/user/userModel";
import { RecapAbsensi } from "../services/recap/recapModel";
import {Perizinan} from "../services/perizinan/perizinanModel";
import { JadwalHusus } from "../services/jadwalHusus/jadwalHususModel";

async function Sync():Promise<void> {
    try{
        db.authenticate();
        console.info('connected')
        await Ptk.sync({alter:true})
        await Absensi.sync({alter:true})
        await noUrutPegawai.sync({alter:true})
        await Card.sync({alter:true})
        await Jadwal.sync({alter:true})
        await ListJadwal.sync({alter:true})
        await JadwalSementara.sync({alter:true})
        await User.sync({alter:true})
        await RecapAbsensi.sync({alter:true})
        await Perizinan.sync({alter:true})
        await JadwalHusus.sync({alter:true})
       
    }catch(e){
        console.warn(e)
    }
}

export default Sync;
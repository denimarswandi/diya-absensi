import { DataTypes, Model, InferAttributes, InferCreationAttributes, CreationOptional } from "sequelize";
import db from "../../config/database";

interface RecapAbsensi extends Model<InferAttributes<RecapAbsensi>, InferCreationAttributes<RecapAbsensi>>{
    uuid:CreationOptional<string>;
    ket: string;
    tanggal:Date;
    kodeRekap:number;
    tahun:number;

}


export const RecapAbsensi = db.define<RecapAbsensi>('recapAbsensi',{
    uuid:{
        type:DataTypes.UUID,
        primaryKey:true,
        defaultValue:DataTypes.UUIDV4
    },
    ket:{
        type:DataTypes.STRING(10),
        allowNull:false
    },
    tanggal:{
        type:DataTypes.DATEONLY,
        allowNull:false
    },
      kodeRekap:{
        type:DataTypes.INTEGER,
        allowNull:false
    },
    tahun:{
        type:DataTypes.INTEGER,
        allowNull:false
    },

})
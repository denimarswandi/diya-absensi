import { Request, Response } from "express";
import { JadwalSementara } from "./jadwalSementaraModel";

function generateData(data:any, uuid:string):Promise<any>{
    return new Promise(async(resolve, reject)=>{
        try{
            data.map((d:any)=>{
                JadwalSementara.create({
                    datang:`${d.datang}:59`,
                    pulang:`${d.pulang}:59`, 
                    hari:d.hari.toLowerCase(),
                    start:d.start,
                    end:d.end,
                    list_jadwal_uuid: uuid
                })
            })
            resolve({msg:'ok'})
        }catch(_){
            reject({
                msg:'uups something wrong'
            })
        }
    })
}

class JadwalSementaraController{
    public async create(req:Request, res:Response):Promise<Response>{
        // const {datang, pulang, hari, lembaga, start, end} = req.body;
        // await JadwalSementara.create({
        //     datang, pulang, hari:hari.toLowerCase(), lembaga,start, end
        // })
        await generateData(req.body.jadwal, req.params.uuid)
        return res.json({msg:'created'})
    }

    public async getByDate(req:Request, res:Response):Promise<Response>{
        const start = req.params.start
        const end = req.params.end
        const data = await JadwalSementara.findAll({where:{
            start:start,
            end:end
        }})
        return res.json(data)
    }

}

export default new JadwalSementaraController
import { adminAuth, checkSuperAdmin } from "../../middleware/admin";
import BaseRouter from "../../router/baseRouter";
import User from './userController';

class UserRouter extends BaseRouter{
    routes(): void {
        this.router.post('/', adminAuth, checkSuperAdmin, User.register)
        this.router.post('/login', User.login)
        this.router.get('/', User.get)
    }
}

export default new UserRouter().router
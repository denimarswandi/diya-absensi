import BaseRouter from "../../router/baseRouter";
import JadwalHusus from "./jadwalHususController";
import { jadwalHususCreateValidator } from "./jadwalHususValidator";

class JadwalHususRouter extends BaseRouter{
    routes(): void {
        this.router.get('/', JadwalHusus.getAll)
        this.router.post('/',jadwalHususCreateValidator, JadwalHusus.create)
    }
}

export default new JadwalHususRouter().router
import { DataTypes } from "sequelize";
import db from '../../config/database'
import { Absensi } from "../absensi/absensiModel";
import { Card } from "../card/cardModel";
import { RecapAbsensi } from "../recap/recapModel";
import  {Perizinan}  from "../perizinan/perizinanModel";
import { JadwalHusus } from "../jadwalHusus/jadwalHususModel";

export const Ptk = db.define('ptk',{
    uuid:{
        type:DataTypes.UUID,
        primaryKey:true,
        defaultValue:DataTypes.UUIDV4
    },
    nupy:{
        type:DataTypes.STRING(255),
        allowNull:false
    },
    nama:{
        type:DataTypes.STRING(255),
        allowNull:false
    },
    tanggalLahir:{
        type:DataTypes.DATEONLY,
        allowNull:false
    },
    alamatLengkap:{
        type:DataTypes.STRING(255),
        allowNull:false
    },
    gender:{
        type:DataTypes.CHAR(1),
        allowNull:false
    },
    statusNikah:{
        type:DataTypes.CHAR(10)
    },
    golonganDarah:{
        type:DataTypes.CHAR(2)
    },
    telpon:{
        type:DataTypes.STRING(15),
    },
    statusTempatTinggal:{
        type:DataTypes.STRING(100)
    },
    pendidikanTerakhir:{
        type:DataTypes.STRING(100)
    },
    unitKerja:{
        type:DataTypes.STRING(100)
    },
    statusKepegawaian:{
        type:DataTypes.STRING(100)
    },
    tugasPokok:{
        type:DataTypes.STRING(100)
    },
    tugasTambahan:{
        type:DataTypes.STRING(100)
    },
    tmt:{
        type:DataTypes.DATEONLY
    }

},{
    paranoid:true
})

Ptk.hasMany(Absensi, {foreignKey:'ptk_uuid'})
Absensi.belongsTo(Ptk, {foreignKey:'ptk_uuid'})
Ptk.hasMany(Card, {foreignKey:'ptk_uuid'})
Card.belongsTo(Ptk, {foreignKey:'ptk_uuid'})
Ptk.hasMany(RecapAbsensi, {foreignKey:'ptk_uuid'})
RecapAbsensi.belongsTo(Ptk, {foreignKey:'ptk_uuid'})
Ptk.hasMany(Perizinan, {foreignKey:'ptk_uuid'})
Absensi.belongsTo(Ptk, {foreignKey:'ptk_uuid'})
Ptk.hasMany(JadwalHusus, {foreignKey:'ptk_uuid'})
JadwalHusus.belongsTo(Ptk, {foreignKey:'ptk_uuid'})
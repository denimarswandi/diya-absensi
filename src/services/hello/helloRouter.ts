import Hello from './helloController';
import BaseRouter from '../../router/baseRouter';

class HelloRouter extends BaseRouter{
    routes(): void {
        this.router.get('/', Hello.index)
    }
}

export default new HelloRouter().router
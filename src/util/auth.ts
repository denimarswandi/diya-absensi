import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import {configEnv} from './envLoacte'

configEnv()

class Authenticate{
    public static passwordHash(password:string):Promise<string>{
        return bcrypt.hash(password, 10)
    }

    public static passwordCompare = async(text:string, encryptText:string):Promise<boolean>=>{
        return await bcrypt.compare(text, encryptText)
    }

    public static generateToken(id:string):string{
        const token:string = jwt.sign({id}, process.env.JWT_SECRET_KEY||'bismillah212',{
            expiresIn:'6h'
        })
        return token
    }

}

export default Authenticate;
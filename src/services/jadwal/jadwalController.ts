import { Request, Response } from "express";
import { Jadwal } from "./jadwalModel";

function generateListAbsen(data: any, uuid: string): Promise<any> {
  return new Promise(async (resolve, _) => {
    for (let i = 0; i < data.length; i++) {
      if(await Jadwal.findOne({where:{list_jadwal_uuid:uuid, hari:data[i].hari.toLowerCase()}})){
        await Jadwal.update({
          datang:`${data[i].datang}:59`,
          pulang:`${data[i].pulang}:59`
        },{where:{
          list_jadwal_uuid:uuid,
          hari:data[i].hari.toLowerCase()
        }})
      }else{
        await Jadwal.create({
          datang:`${data[i].datang}:59`,
          pulang:`${data[i].pulang}:59`,
          hari:data[i].hari.toLowerCase(),
          aktif:data[i].aktif,
          list_jadwal_uuid:uuid
        })
      }
    }
   resolve('ok')
  });
}

function changeTimeList(data:any):Promise<any>{
  return new Promise(async(resolve,_)=>{
    let newData:any = []
    data.map((d:any)=>{
      newData.push({
        datang:d.datang.toString().substr(0,5),
        pulang:d.pulang.toString().substr(0,5),
        hari:d.hari
      })
    })
    resolve(newData)
  })

}

class JadwalController {
  public async getAll(req: Request, res: Response): Promise<Response> {
    const jadwal = await Jadwal.findAll({where:{
      list_jadwal_uuid: req.params.uuid
    }, attributes:['datang', 'pulang', 'hari']})
    const dd = await changeTimeList(jadwal)
    return res.json(dd)
  }

  public async create(req: Request, res: Response): Promise<Response> {
    const { jadwal } = req.body;
    const uuidLembaga = req.params.uuid;
    try {
      await generateListAbsen(jadwal, uuidLembaga);
      return res.json({ msg: "created" });
    } catch (e) {
      return res.status(400).json({ msg: "uups something wrong" });
    }
  }
}

export default new JadwalController();

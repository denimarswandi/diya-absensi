import { DataTypes } from "sequelize";
import db from '../../config/database';

export const Absensi = db.define('absensi', {
    uuid:{
        type:DataTypes.UUID,
        primaryKey:true,
        defaultValue:DataTypes.UUIDV4,
    },
    absenTime:{
        type:DataTypes.DATE,
        allowNull:false
    }
})
import { DataTypes, Model, InferAttributes, InferCreationAttributes, CreationOptional } from "sequelize";
import db from '../../config/database';

interface Perizinan extends Model<InferAttributes<Perizinan>, InferCreationAttributes<Perizinan, {omit:'uuid'}>>{
    uuid:CreationOptional<string>;
    ket: string;
    dariTanggal: Date;
    sampaiTanggal:Date;
    ptk_uuid?:string;

}

export const Perizinan = db.define<Perizinan>('perizinan', {
    uuid:{
        type:DataTypes.UUID,
        primaryKey:true,
        defaultValue:DataTypes.UUIDV4
    },
    ket:{
        type:DataTypes.STRING,
        allowNull:false
    },
    dariTanggal:{
        type:DataTypes.DATEONLY,
        allowNull:false
    },
    sampaiTanggal:{
      type:DataTypes.DATEONLY,
      allowNull:false
    }
})

// const Perizinan = db.define<Perizinan>('perizinan', {
//     uuid:{
//         type:DataTypes.UUID,
//         primaryKey:true,
//         defaultValue:DataTypes.UUIDV4
//     },
//     ket:{
//         type:DataTypes.STRING,
//         allowNull:false
//     },
//     dariTanggal:{
//         type:DataTypes.DATEONLY,
//         allowNull:false
//     },
//     sampaiTanggal:{
//         type:DataTypes.DATEONLY,
//         allowNull:false
//     },

// })


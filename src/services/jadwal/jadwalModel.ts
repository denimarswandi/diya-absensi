import { DataTypes } from "sequelize";
import db from '../../config/database';

export const Jadwal =  db.define('jadwal', {
    uuid:{
        type:DataTypes.UUID,
        primaryKey:true,
        defaultValue:DataTypes.UUIDV4
    },
    datang:{
        type:DataTypes.TIME,
        allowNull:false
    },
    pulang:{
        type:DataTypes.TIME,
        allowNull:false
    },
    hari:{
        type:DataTypes.STRING(100),
        allowNull:false
    },
    aktif:{
        type:DataTypes.BOOLEAN,
        allowNull:false
    }
},{
    paranoid:true
})

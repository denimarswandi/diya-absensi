import express from 'express';
import Hello from '../services/hello/helloRouter';
import PTK from '../services/ptk/router'
import Card from '../services/card/cardRouter'
import Absensi from '../services/absensi/absensiRouter'
import Jadwal from '../services/jadwal/jadwalRouter'
import ListJadwal from '../services/lsitJadwal/listJadwalRouter';
import JadwalSementara from '../services/jadwalSementara/jadwalSementaraRouter';
import User from '../services/user/userRouter'
import Recap from '../services/recap/recapRouter'
import Perizinan from '../services/perizinan/perizinanRouter';
import JadwalHusus from '../services/jadwalHusus/jadwalHususRouter';

export const routes = express();
routes.use('/hello', Hello)
routes.use('/ptk', PTK)
routes.use('/device', Card)
routes.use('/absensi', Absensi)
routes.use('/jadwal', Jadwal)
routes.use('/list/jadwal', ListJadwal)
routes.use('/sementara/jadwal', JadwalSementara)
routes.use('/user', User)
routes.use('/recap', Recap)
routes.use('/perizinan',Perizinan )
routes.use('/husus/jadwal', JadwalHusus)

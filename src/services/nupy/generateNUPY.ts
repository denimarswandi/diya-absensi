import {noUrutPegawai} from "./noUrutModel"
import {v4 as uuidv4} from 'uuid'

const dataLength = (nourut:number)=>{
    const lengthUrut = String(nourut).length
    if(lengthUrut===1){
        return `00${String(nourut)}`
    }else if(lengthUrut===2){
        return `0${String(nourut)}`
    }else{
        return `${String(nourut)}`
    }
    
}

export const generateNupy = (date: Date, gender: string)=>{
    let convertGender = gender.toLocaleLowerCase()==='l' ? 1 : 2
    let splitDate = String(date).split('-')
    const newDate = new Date()
    return new Promise(async(resolve, reject)=>{
        try{
            const data:any = await noUrutPegawai.findOne({
                where:{
                    tahun:newDate.getFullYear()
                }
            })
           if(data===null){
            await noUrutPegawai.create({
                uuid: uuidv4(),
                no:1,
                tahun:newDate.getFullYear()
            })
            resolve(`${splitDate[0]}${splitDate[1]}${newDate.getFullYear()}${convertGender}00${String(1)}`)
           }
           else{
            let noUrut = data.no
            await noUrutPegawai.update({no:noUrut + 1}, {where:{
                tahun:newDate.getFullYear()
            }})
            resolve(resolve(`${splitDate[0]}${splitDate[1]}${newDate.getFullYear()}${convertGender}${dataLength(noUrut+1)}`))
           }
        }catch(err){
            reject(err)
        }
    })
    
}
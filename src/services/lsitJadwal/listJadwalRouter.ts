import BaseRouter from "../../router/baseRouter";
import listJadwalController from "./listJadwalController";
import ListJadwal from "./listJadwalController";
import { listJadwalCreateValidator } from "./listJadwalValidator";
import {adminAuth, checkSuperAdmin} from '../../middleware/admin'
class ListJadwalRouter extends BaseRouter{
    routes(): void {
        this.router.get('/', adminAuth,checkSuperAdmin,  ListJadwal.jadwalList)
        this.router.post('/',adminAuth, checkSuperAdmin, listJadwalCreateValidator,ListJadwal.post)
        this.router.get('/:uuid',adminAuth, checkSuperAdmin, listJadwalController.getOne)
        this.router.put('/:uuid',adminAuth, checkSuperAdmin, listJadwalController.update)
        // this.router.get('/user/data',adminAuth, listJadwalController.jadwalList)
    }
}

export default new ListJadwalRouter().router
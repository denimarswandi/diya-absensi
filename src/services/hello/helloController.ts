import { Request, Response } from "express";

class Hello{
    public async index(req:Request, res:Response):Promise<Response>{
        return res.json({msg:'hello'})
    }
}

export default new Hello
import { Request, Response } from "express";
import { Card } from "./cardModel";

class CardController{
    public async create(req:Request, res:Response):Promise<Response>{
        const {tagCode, nupy} = req.body
        const uuid_ = req.params.uuid
        await Card.create({tagCode, nupy, ptk_uuid:uuid_})
        return res.json({msg:'created'})
    }
}

export default new CardController
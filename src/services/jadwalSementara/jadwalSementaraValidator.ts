import { check } from "express-validator";
import { nextFunc } from "../../middleware/loadValidator";

export const jadwalSementaraCreateValidator = [
    check('jadwal').isArray({min:1, max:7}),
    check('jadwal[*].datang').exists().matches(/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/).withMessage('uups not valid'),
    check('jadwal[*].pulang').exists().matches(/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/).withMessage('uups not valid'),
    check('jadwal[*].hari').exists().isString().not().isEmpty(),
    check('jadwal[*].start').exists().isISO8601().toDate().not().isEmpty(),
    check('jadwal[*].end').exists().isISO8601().toDate().not().isEmpty(),
    nextFunc
]

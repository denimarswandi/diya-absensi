import BaseRouter from "../../router/baseRouter";
import Perizinan from "./perizinanController";
import { perizinanValidator } from "./perizinanValidator";


class PerizinanRouter extends BaseRouter{
    routes(): void {
        this.router.get('/', Perizinan.getAll)
        this.router.post('/:uuid', perizinanValidator, Perizinan.create)
    }
}

export default new PerizinanRouter().router

import { Sequelize } from "sequelize";
import dotenv from 'dotenv';
// import path from 'path'

dotenv.config();
//dotenv.config({ path: '/home/public3/diya-absensi/.env'});

const db = new Sequelize(process.env.DB_NAME || '', process.env.DB_USER || '', process.env.DB_PASSWORD || '', {
    host: process.env.DB_HOST,
    dialect: 'mysql',
    define:{
        freezeTableName: true
    },
    logging:false
})

export default db;
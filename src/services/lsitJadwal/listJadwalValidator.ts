import { check } from "express-validator";
import { nextFunc } from "../../middleware/loadValidator";

export const listJadwalCreateValidator = [
    check('lembaga').exists().isString().not().isEmpty(),
    nextFunc
]
import { DataTypes } from "sequelize";
import db from '../../config/database';


export const noUrutPegawai = db.define('noUrutPegawai', {
    uuid:{
        type:DataTypes.UUID,
        primaryKey:true
    },
    no:{
        type:DataTypes.INTEGER,
        allowNull:false
    },
    tahun:{
        type:DataTypes.INTEGER,
        allowNull:false
    }
})


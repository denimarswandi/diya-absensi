import { Request, Response } from "express";
import { ListJadwal } from "./listJadwalModel";
import {User} from '../user/userModel'

class ListJadwalController{
    public async get(req:Request, res:Response):Promise<Response>{
        const listJadwal = await ListJadwal.findAll({attributes:['uuid', 'lembaga']})
        return res.json(listJadwal)
    }

    public async jadwalList(req:Request, res:Response):Promise<Response>{
        const credential = req.app.locals.credential
        const user:any = await User.findOne({
            where:{
                uuid:credential.id
            }
        })
        // console.log(credential.id)
        // console.log(user)
        if(user.admin){
            const listJadwal = await ListJadwal.findAll({attributes:['uuid', 'lembaga']})
            return res.json(listJadwal)
        }
        else{
            const listJadwal = await ListJadwal.findAll({
                where:{
                    lembaga:user.lembaga
                },
                attributes:['uuid', 'lembaga']
            })
            return res.json(listJadwal)
        }
     
        
    }
    
    public async post(req:Request, res:Response):Promise<Response>{
        const data = req.body;
        await ListJadwal.create({lembaga:data.lembaga})
        return res.json({msg:'created'})
    }

    public async getOne(req:Request, res:Response):Promise<Response>{
        return res.json(await ListJadwal.findOne({where:{uuid:req.params.uuid}, attributes:['lembaga']}))
    }

    public async update(req:Request, res:Response):Promise<Response>{
        const {lembaga} = req.body
        await ListJadwal.update({lembaga},{where:{uuid:req.params.uuid}})
        return res.json({msg:'updated'})
    }

}

export default new ListJadwalController
import { Model, InferAttributes, InferCreationAttributes, CreationOptional, DataTypes } from "sequelize";
import db from '../../config/database'

interface JadwalHusus extends Model<InferAttributes<JadwalHusus>, InferCreationAttributes<JadwalHusus, {omit:'uuid'}>>{
    uuid: CreationOptional<string>;
    masuk:Date;
    pulang:Date;
    jamMasuk:string;
    jamPulang:string;
    ptk_uuid?:string;
}

export const JadwalHusus = db.define<JadwalHusus>('jadwalHusus',{
    uuid: {
        type:DataTypes.UUID,
        primaryKey:true,
        defaultValue:DataTypes.UUIDV4
    },
    masuk:{
        type:DataTypes.DATEONLY,
        allowNull:false
    },
    pulang:{
        type:DataTypes.DATEONLY,
        allowNull:false
    },
    jamMasuk:{
        type:DataTypes.TIME,
        allowNull:false
    },
    jamPulang:{
        type:DataTypes.TIME,
        allowNull:false
    }
})
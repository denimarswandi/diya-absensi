import BaseRouter from "../../router/baseRouter";
import JadwalSementara from './jadwalSementaraController'
import { jadwalSementaraCreateValidator } from "./jadwalSementaraValidator";

class JadwalSementaraRouter extends BaseRouter{
    routes(): void {
        this.router.post('/:uuid',jadwalSementaraCreateValidator, JadwalSementara.create)
        this.router.get('/:start/:end/:uuid', JadwalSementara.getByDate)
    }
}

export default new JadwalSementaraRouter().router
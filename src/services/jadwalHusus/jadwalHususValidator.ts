import { check } from "express-validator";
import { nextFunc } from "../../middleware/loadValidator";


export const jadwalHususCreateValidator = [
    check('nupy').exists().isNumeric().not().isEmpty(),
    check('jadwal').isArray({min:1, max:100}),
    check('jadwal[*].masuk').isDate().withMessage("Invalid day received"),
    check('jadwal[*].pulang').isDate(),
    check('jadwal[*].jamMasuk').exists().matches(/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/).withMessage('upps not valid'),
    check('jadwal[*].jamPulang').exists().matches(/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/).withMessage('upps not valid'),
    nextFunc


]
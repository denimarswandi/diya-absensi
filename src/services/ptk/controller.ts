import { Request, Response } from "express";
import { Ptk } from "./model";
import { generateNupy } from "../nupy/generateNUPY";
import { Card } from "../card/cardModel";

class PtkController {
    public async getAll(req:Request, res:Response):Promise<Response>{
        return res.json(await Ptk.findAll({include:[
            {
                model:Card
            }
        ]}))
    }

    

    public async create(req:Request, res:Response):Promise<Response>{
        const data = req.body;
        await Ptk.create({
            nupy:await generateNupy(data.tanggalLahir, data.gender),
            nama:data.nama,
            tanggalLahir:data.tanggalLahir,
            alamatLengkap:data.alamatLengkap,
            gender:data.gender,
            statusNikah:data.statusNikah,
            golonganDarah:data.golonganDarah,
            telpon:data.telpon,
            statusTempatTinggal:data.statusTempatTinggal,
            pendidikanTerakhir:data.pendidikanTerakhir,
            unitKerja:data.unitKerja,
            statusKepegawaian:data.statusKepegawaian,
            tugasPokok:data.tugasPokok,
            tugasTambahan:data.tugasTambahan,
            tmt:data.tmt
        })
        return res.json({msg:'created'})
    }
}

export default new PtkController
import { Request, Response } from "express";
import { JadwalHusus } from "./jadwalHususModel";
import { Ptk } from "../ptk/model";
class JadwalHususController{
    public async getAll(req:Request, res:Response):Promise<Response>{
        return res.json({msg:'hello'})
    }

    public async create(req:Request, res:Response):Promise<Response>{
        const {nupy, jadwal} = req.body;
        const ptk:any = await  Ptk.findOne({where:{nupy:nupy}})
        jadwal.map(async (d:any)=>{
            await JadwalHusus.create({
                masuk:d.masuk,
                pulang:d.pulang,
                jamMasuk:`${d.jamMasuk}:59`,
                jamPulang:`${d.jamPulang}:59`,
                ptk_uuid:ptk.uuid
            })
        })
        return res.json({msg:'created'})
    }
}

export default new JadwalHususController
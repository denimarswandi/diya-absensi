import BaseRouter from "../../router/baseRouter";
import Absensi from './absensiController'

class AbsensiRouter extends BaseRouter{
    routes(): void {
        this.router.get('/', Absensi.getToday)
        this.router.post('/', Absensi.absenToDay)
    }
}

export default new AbsensiRouter().router
import pymysql

def getConSpp():
  db = pymysql.connect(host='localhost', user='root', password='ayambakarkecap212', database='absensi')
  return db

def commitSQL(sql, params=[]):
  con = getConSpp()
  cur = con.cursor()
  try:
    cur.execute(sql, params)
    con.commit()
  except Exception as e:
    con.rollback()
    print(e)
  finally:
    con.close()

def getSingleQuery(sql, params=[]):
  con = getConSpp()
  cur = con.cursor(pymysql.cursors.DictCursor)
  try:
    cur.execute(sql, params)
    res = cur.fetchone()
    return res
  except Exception as e:
    print(e)
  finally:
    con.close()

def getMultiQuery(sql, params=[]):
  con = getConSpp()
  cur = con.cursor(pymysql.cursors.DictCursor)
  try:
    cur.execute(sql, params)
    res = cur.fetchall()
    return res
  except Exception as e:
    print(e)
  finally:
    con.close()

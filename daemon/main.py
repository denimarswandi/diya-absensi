from datetime import datetime
from dateutil import tz
from database import commitSQL, getMultiQuery, getSingleQuery
import calendar
import time
import uuid


def tahunKabisat(tahun):
  if(tahun % 4) ==0:
    if(tahun %100)==0:
      if(tahun % 400)==0:
        return True
      else:
        return False
    else:
      return True
  else:
    return False

def convertTime(timeString):
  return datetime.strptime(timeString, '%Y-%m-%d %H:%M:%S')

def convertTimeToLocal(time_):
  tz_zone = tz.gettz('Asia/Makassar')
  tz_utc = tz.gettz('UTC')
  time_ = time_.replace(tzinfo=tz_utc)
  return time_.astimezone(tz_zone)

def convertTimeTOUtc(time_):
  tz_utc = tz.gettz('UTC')
  tz_gmt = tz.gettz('Asia/Makassar')
  time_ = time_.replace(tzinfo=tz_gmt)
  return time_.astimezone(tz_utc)

def convertTimeToLocalLocal(time_):
  tz_zone = tz.gettz('Asia/Makassar')
  return time_.replace(tzinfo=tz_zone)

def convertTimeToLocalZone(time_):
  tz_zone = tz.gettz('Asia/Makassar')
  return time_.astimezone(tz_zone)

def convertTimeToLocalUtc(time_):
  tz_zone = tz.gettz('UTC')
  return time_.astimezone(tz_zone)

  

def getRangeMonth(keyMonth):
  if(keyMonth==0 or keyMonth==12):
    return 31
  elif(keyMonth==1):
    return 31
  elif(keyMonth==2):
    kabisat = tahunKabisat(datetime.now().year)
    if kabisat:
      return 29
    return 28
  elif(keyMonth==3):
    return 31
  elif(keyMonth==4):
    return 30
  elif(keyMonth==5):
    return 31
  elif(keyMonth==6):
    return 30
  elif(keyMonth==7):
    return 31
  elif(keyMonth==8):
    return 31
  elif(keyMonth==9):
    return 30
  elif(keyMonth==10):
    return 31
  elif(keyMonth==11):
    return 30

def makeTwoDigits(data):

  if(len(str(data))==1):
    return '0{}'.format(data)
  return data

def generateList():
  start = 25
  end=26
  range_ = []
  month_list = ['12', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11','12']
  month = datetime.now().month
  # month = 12
  years = datetime.now().year
  for x in range(start, getRangeMonth(month-1)+1):
    if(month-1==0):
      range_.append('{}-{}-{}'.format(years-1, month_list[0], x))
    else:
      range_.append('{}-{}-{}'.format(years, month_list[month-1], x))

  for x in range(1, end+1):
    range_.append('{}-{}-{}'.format(years, month_list[month], makeTwoDigits(x)))
  return range_


def getPtk():
  sql = """select * from ptk where deletedAt is null"""
  return getMultiQuery(sql, [])

def getAbsensi(uuid, start, end):
  sql = """select absenTime from absensi where ptk_uuid=%s and (absenTime between %s and %s) order by absenTime asc"""
  params = [uuid, start, end]
  return getMultiQuery(sql, params)

def getJadwal(lembaga, hari):
  sql = """select list_jadwal.lembaga, jadwal.hari, jadwal.datang, jadwal.pulang, jadwal.list_jadwal_uuid from jadwal, list_jadwal where jadwal.list_jadwal_uuid = list_jadwal.uuid and list_jadwal.lembaga=%s and jadwal.hari=%s"""
  params = [lembaga, hari]
  return getSingleQuery(sql, params)

def saveRecap(uuid, tanggal, createdAt, updatedAt, ket, ptk_uuid, kodeRekap, tahun):
  sql = """insert into recapAbsensi(uuid, tanggal, createdAt, updatedAt, ket, ptk_uuid, kodeRekap, tahun) values(%s, %s, %s, %s, %s, %s, %s, %s)"""
  params = [uuid, tanggal, createdAt, updatedAt, ket, ptk_uuid, kodeRekap, tahun]
  commitSQL(sql, params)

def getPerizinan(tanggal, uuid):
  sql = """select * from perizinan where dariTanggal <= %s and sampaiTanggal >=%s and ptk_uuid=%s"""
  params = [tanggal, tanggal, uuid]
  return getSingleQuery(sql, params)

if __name__=='__main__':
  start_time = time.time()
  monthToDay = datetime.now().month
  yearToDay = datetime.now().year
  ptk = getPtk()
  listDay = {
    'monday':'senin',
    'tuesday':'selasa',
    'wednesday':'rabu',
    'thursday':'kamis',
    'friday':'jumat',
    'saturday':'sabtu',
    'sunday':'ahad'
  }
  listLoop = generateList()
  for i in ptk:
    print(i['nama'])
    print(i['unitKerja'])
    statusKehadiran = '-'
    for j in listLoop:
      start = convertTimeTOUtc(convertTime('{} 00:00:00'.format(j)))
      end = convertTimeTOUtc(convertTime('{} 23:59:59'.format(j)))
      absen = getAbsensi(i['uuid'], start, end)
      localTime = convertTimeToLocalZone(start)
      nameEn = str(calendar.day_name[localTime.weekday()]).lower()
      day = listDay[nameEn]
      # print(j)
      # print(start)
      # print(localTime)
      # print(day)
      # print('&&&&&')
      jadwal = getJadwal(i['unitKerja'], day)
      time.sleep(0.01)
      # print(monthToDay)
      # print(yearToDay)
      perizinanPtk = getPerizinan(j, i['uuid'])
      if perizinanPtk:
        saveRecap(str(uuid.uuid4()), j, timeNow, timeNow, perizinanPtk['ket'], i['uuid'], monthToDay-1, yearToDay)
        print(j)
        time.sleep(10)
        continue


      if jadwal:
        if absen:
          if(len(absen)>=2):
            datang_ = convertTimeToLocal(convertTime(str(absen[0]['absenTime'])))
            pulang_ = convertTimeToLocal(convertTime(str(absen[len(absen)-1]['absenTime'])))
            datang_jadwal = convertTimeToLocalZone(convertTime('{} {}'.format(j, jadwal['datang'])))
            pulang_jadwal = convertTimeToLocalZone(convertTime('{} {}'.format(j, jadwal['pulang'])))
            print('--absen not convert--')
            print(convertTime(str(absen[0]['absenTime'])))
            print(convertTime(str(absen[len(absen)-1]['absenTime'])))
            print('---absen---')
            print(datang_)
            print(pulang_)
            print('---jadwal---')
            print(datang_jadwal)
            print(pulang_jadwal)
          
            if(datang_ <= datang_jadwal) and (pulang_jadwal <= pulang_):
              statusKehadiran='v'
              print('V')
            elif(datang_ >= datang_jadwal) and (pulang_jadwal <= pulang_):
              statusKehadiran='TL'
              print('TL')
            elif(datang_ <= datang_jadwal) and (pulang_jadwal >=pulang_):
              statusKehadiran='TB'
              print('TB')
            elif(datang_ >= datang_jadwal) and (pulang_jadwal>=pulang_):
              statusKehadiran='TLB'
              print('TLB')
          
          elif(len(absen)==1):
            # print(';;;;')
            # print(str(absen[0]['absenTime']))
            # print('{} {}'.format(j, jadwal['datang']))
            # print('o')
            # print(convertTimeToLocal(convertTime(str(absen[0]['absenTime']))))
            # print(convertTimeToLocalZone(convertTime('{} {}'.format(j, jadwal['datang']))))
            if( convertTimeToLocal(convertTime(str(absen[0]['absenTime'])))<=convertTimeToLocalZone(convertTime('{} {}'.format(j, jadwal['datang'])))):
              print(convertTimeToLocal(convertTime(str(absen[0]['absenTime']))))
              print(convertTimeToLocalZone(convertTime('{} {}'.format(j, jadwal['datang']))))
              print('TB')
              statusKehadiran='TB'
            else:
              statusKehadiran='TLB'
              print('TLB')
        else:
          statusKehadiran='x'
          print('x')
      else:
        statusKehadiran = 'L'
        print('L')
      timeNow = datetime.now()
      saveRecap(str(uuid.uuid4()), j, timeNow, timeNow, statusKehadiran, i['uuid'], monthToDay-1, yearToDay)
      print('____________')
    print('*************')
    print('{} seconds'.format(time.time()-start_time))
    
  #   absen = getAbsensi(i['uuid'], ) 
  #   if absen:
  #     if(len(absen)>=2):
        
  #       datang = absen[0]
  #       pulang = absen[len(absen)-1]
  #       print(convertTimeToLocal(convertTime(str(datang['absenTime']))))
  #       print(convertTimeToLocal(convertTime(str(pulang['absenTime']))))
  #       aa = convertTime(str(pulang['absenTime']))
  #       nameEn = str(calendar.day_name[aa.weekday()]).lower()
  #       jadwal = getJadwal(i['unitKerja'], listDay[nameEn])
  #       print(jadwal)
  #       print('ok')
  #     elif(len(absen)==1):
  #       datang = absen[0]
  #       print('mmm')
  #   else:
  #     print('X')
  #   print('______')
    
  
  
  # # a = convertTimeToLocal(convertTime('2023-01-01 00:00:00'))
  # # b = convertTimeToLocalLocal(convertTime('2023-01-01 00:11:00'))
  # # print(a)
  # # print(b)
  # # print(a>b)


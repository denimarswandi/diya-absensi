import { Request, Response } from "express";
import { Absensi } from "./absensiModel";
import { Card } from "../card/cardModel";
import { recapToday } from "./recapToday";
class AbsensiController {
    public async getToday(req:Request, res:Response):Promise<Response>{
        return res.json(await recapToday('2023-02-09 00:00:00', '2023-02-09 23:59:00'))
           
    }
    public async absenToDay(req:Request, res:Response):Promise<Response>{
        const {tagCode, absenTime} = req.body;
        const card:any = await Card.findOne({where:{tagCode}, attributes:['ptk_uuid']})
        if(card){
            await Absensi.create({tagCode, absenTime, ptk_uuid:card.ptk_uuid})
            return res.json({msg:'created data'})
        }
        return res.status(404).json({msg:'not found'})
    }
}

export default new AbsensiController
import { NextFunction, Request, Response } from "express";

export default function Cache(req:Request, res:Response, next:NextFunction){
    res.set('Cache-Control', 'no-store')
    next()
}
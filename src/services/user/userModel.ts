import { DataTypes } from "sequelize";
import db from '../../config/database'

export const User = db.define('user', {
    uuid:{
        type:DataTypes.UUID,
        primaryKey:true,
        defaultValue:DataTypes.UUIDV4
    },
    nama:{
        type:DataTypes.STRING(255),
        allowNull:false,
    },
    username:{
        type:DataTypes.STRING(255),
        allowNull:false
    },
    password:{
        type:DataTypes.STRING(255),
        allowNull:false
    },
    lembaga:{
        type:DataTypes.STRING(50),
        allowNull:false
    },
    admin:{
        type:DataTypes.BOOLEAN,
        allowNull:false
    }
},{
    paranoid:true
})
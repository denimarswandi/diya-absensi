import BaseRouter from '../../router/baseRouter';
import Ptk from './controller';
import { ptkCreateValidator } from './validator';
import { adminAuth, checkSuperAdmin } from '../../middleware/admin';

class PtkRouter extends BaseRouter{
    routes(): void {
        this.router.get('/', adminAuth, checkSuperAdmin, Ptk.getAll)
        this.router.post('/', adminAuth, checkSuperAdmin, ptkCreateValidator ,Ptk.create)
    }
}

export default new PtkRouter().router
import Sync from "./config/syncModel";
import express , {Application} from 'express';
import morgan from 'morgan';
import compression from 'compression';
import helmet from 'helmet';
import cors from 'cors';
import {routes} from "./router/index";
import Cache from './middleware/cache'
// import Cache from '../src.dis/middlewers/cache'

const app:Application = express();
const port:number = 8080;
app.use(Cache)
app.use(express.json());
app.use(morgan('dev'));
app.use(compression());
app.use(helmet());
// app.use(cors({
//     origin: ['https://www.section.io', 'https://www.google.com/']
// }))
// app.use(cors({
//     origin: '*'
// }))

app.use(cors());
Sync();

app.use('/', routes)

app.listen(port,()=>{
    console.log(`Run on port -_- ${port}`)
});
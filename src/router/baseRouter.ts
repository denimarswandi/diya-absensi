import {Router} from 'express'
import InterfaceRouter from './interfaceRouter'

abstract class BaseRouter implements InterfaceRouter{
    public router:Router
    constructor(){
        this.router = Router()
        this.routes()
    }

    abstract routes():void
}

export default BaseRouter;
import { check } from "express-validator";
import { nextFunc } from "../../middleware/loadValidator";

export const perizinanValidator = [
    check('ket').exists().isString().not().isEmpty(),
    check('dariTanggal').exists().isISO8601().toDate().not().isEmpty(),
    check('sampaiTanggal').exists().isISO8601().toDate().not().isEmpty(),
    nextFunc
]
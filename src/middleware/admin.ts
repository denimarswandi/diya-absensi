import { Request, Response, NextFunction
 } from "express";
import jwt from 'jsonwebtoken'
import { configEnv } from "../util/envLoacte";
import {User} from '../services/user/userModel'

configEnv()

export const adminAuth = async(req:Request, res:Response, next:NextFunction):Promise<Response|void>=>{
    if(!req.headers.authorization){
        return res.status(401).json({msg:'not authentication'})
    }
    let secretKey = process.env.JWT_SECRET_KEY || 'bismillah212'
    const token:string = req.headers.authorization.split(' ')[1]
    try{
        const dataToken:any = jwt.decode(token)
        const credential:string|object = jwt.verify(token, secretKey)
        if(credential){
            req.app.locals.credential = credential
            next()
        }else{
            return res.status(401).json({msg:'unauthorization'})
        }
    }catch(_){
        return res.status(403).json({msg:'token invalid'})
    }
}

export const checkSuperAdmin = async(req:Request, res:Response, next:NextFunction):Promise<any>=>{
    const credential = req.app.locals.credential

    const user:any = await User.findOne({
        where:{
            uuid:credential.id
        }
    })
    
    if(user.admin){
        next()
    }else{
        return res.status(400).json({msg:'not allowed'})
    }
}
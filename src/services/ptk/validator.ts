import { check } from "express-validator";
import { nextFunc } from "../../middleware/loadValidator";
export const ptkCreateValidator = [
    check('nama').exists().isString().not().isEmpty(),
    check('tanggalLahir').exists().isISO8601().toDate().not().isEmpty(),
    check('alamatLengkap').exists().isString().not().isEmpty(),
    check('gender').exists().isString().not().isEmpty().isIn(['L', 'P']),
    check('statusNikah').exists().isString().not().isEmpty().isIn(['BM', 'M', 'J/D']),
    check('golonganDarah').exists().isString().not().isEmpty().isIn(['A','B', 'O', 'AB']),
    check('telpon').exists().isString().not().isEmpty(),
    check('statusTempatTinggal').exists().isString().not().isEmpty(),
    check('pendidikanTerakhir').exists().isString().not().isEmpty(),
    check('unitKerja').exists().isString().not().isEmpty(),
    check('statusKepegawaian').exists().isString().not().isEmpty(),
    check('tugasPokok').exists().isString().not().isEmpty(),
    check('tugasTambahan').exists().isString().not().isEmpty(),
    check('tmt').exists().isISO8601().toDate(),
    nextFunc
]
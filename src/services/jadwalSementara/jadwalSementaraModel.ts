import {DataTypes} from 'sequelize';
import db from '../../config/database';

export const JadwalSementara = db.define('jadwal_sementara', {
    uuid:{
        type:DataTypes.UUID,
        primaryKey:true, 
        defaultValue:DataTypes.UUIDV4
    },
    datang:{
        type:DataTypes.TIME,
        allowNull:false
    },
    pulang:{
        type:DataTypes.TIME,
        allowNull:false
    },
    hari:{
        type:DataTypes.STRING(100),
        allowNull:false
    },
    start:{
        type:DataTypes.DATEONLY,
        allowNull:false
    },
    end:{
        type:DataTypes.DATEONLY,
        allowNull:false
    }
})
import { DataTypes } from "sequelize";
import db from '../../config/database';
import { Jadwal } from "../jadwal/jadwalModel";
import { JadwalSementara } from "../jadwalSementara/jadwalSementaraModel";

export const ListJadwal = db.define('list_jadwal', {
    uuid:{
        type:DataTypes.UUID,
        primaryKey:true,
        defaultValue:DataTypes.UUIDV4
    },
    lembaga:{
        type:DataTypes.STRING(200),
        allowNull:false
    },
    aktif:{
        type:DataTypes.BOOLEAN,
        allowNull:false,
        defaultValue:true
    }
},{
    paranoid:true
})

ListJadwal.hasMany(Jadwal, {foreignKey:'list_jadwal_uuid'})
Jadwal.belongsTo(ListJadwal, {foreignKey:'list_jadwal_uuid'})
ListJadwal.hasMany(JadwalSementara, {foreignKey:'list_jadwal_uuid'})
JadwalSementara.belongsTo(ListJadwal, {foreignKey:'list_jadwal_uuid'})






